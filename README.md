# metrics-platform-client-java
Metrics client library for the official Wikipedia app for Android.

See the [Metrics Platform](https://wikitech.wikimedia.org/wiki/Metrics_Platform) project page on Wikitech for background.

## Testing
Run tests with `./gradlew test`.

To produce a coverage report, run `./gradlew test jacocoTestReport` and find the resulting report under `build/reports/jacoco/`.

For more details, see https://docs.gradle.org/current/userguide/jacoco_plugin.html.
